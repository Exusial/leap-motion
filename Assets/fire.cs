﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fire : MonoBehaviour
{
    public GameObject gun_base;
    public GameObject gun_hole;
    private gun_attr attrs;

    void Awake(){
        attrs = GetComponent<gun_attr>();
    }

    public void startfire()
    {
        if(attrs.ammo > 0){
            Vector3 base_position = gun_base.transform.position;
            Vector3 hole_position = gun_hole.transform.position;
            Vector3 direction = hole_position - base_position;
            GameObject bullets = (GameObject) Resources.Load("Bullet");
            GameObject newbullet = Instantiate(bullets);
            newbullet.transform.position = hole_position;
            newbullet.GetComponent<move>().direct = direction;
            attrs.ammo -= 1;
            print("start firing!");
            print(attrs.ammo);
        }
        else{
            print("no ammo");
        }
    }

    public void endfire(){
        print("end firing");
    }
}
