﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{

    public Vector3 direct;
    // Update is called once per frame
    void Update()
    {
        Vector3 np = this.transform.position + direct;
        this.transform.position = np;
    }
}
