﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canshoot : MonoBehaviour
{
    // Start is called before the first frame update
    private int hp;
    private int defense;
    
    void Start()
    {
        //对于可被攻击的角色，我们定义如下内容：
        //生命力,防御力
        hp = 100;
        defense = 10;
    }

    public void changeHP(int newhp){
        hp = newhp;
    }

    public void changeDef(int newdef){
        defense = newdef;
    }

    public void takeDamage(int damage){
        float rate = (float)(defense)/(100 + defense);
        int subhp = (int)((1 - rate) * damage);
        subhp = (subhp < 1)? 1 : subhp;
        hp -= subhp;
    }

    public int getHp(){
        return hp;
    }
}
