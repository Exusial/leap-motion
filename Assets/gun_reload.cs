﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class gun_reload : MonoBehaviour
{
    private gun_attr[] all_attrs;
    private Text text;
    void Start()
    {
        text = GetComponent<Text>();
        all_attrs = GameObject.FindObjectsOfType<gun_attr>();
    }
    // Update is called once per frame
    void Update()
    {
        foreach(gun_attr attr in all_attrs){
            if(attr.is_used){
                int ammo = attr.ammo;
                string name = attr.gun_name;
                text.text =  string.Format("{0}     AMMO:{1}",name,ammo);
                break;
            }
        }
    }
}
