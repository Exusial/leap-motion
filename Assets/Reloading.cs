﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reloading : MonoBehaviour
{
    public gun_attr attrs;
    public int ammos;
    public void reload(){
        attrs.ammo = ammos;
    }
}
