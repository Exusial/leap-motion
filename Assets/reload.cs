﻿using Leap.Unity;
using Leap.Unity.Interaction;
using UnityEngine;

/// <summary>
/// This simple script changes the color of an InteractionBehaviour as
/// a function of its distance to the palm of the closest hand that is
/// hovering nearby.
/// </summary>
[AddComponentMenu("")]
[RequireComponent(typeof(InteractionBehaviour))]
public class reload : MonoBehaviour {


  private InteractionBehaviour _intObj;
  private gun_attr attrs;
  public int ammos;
  void Start() {
    _intObj = GetComponent<InteractionBehaviour>();
    attrs = GetComponent<gun_attr>();
  }

  void Update() {
      // The target color for the Interaction object will be determined by various simple state checks.

      // "Primary hover" is a special kind of hover state that an InteractionBehaviour can
      // only have if an InteractionHand's thumb, index, or middle finger is closer to it
      // than any other interaction object.
      if(_intObj.isPrimaryHovered){
          print("55555");
      }
      if(_intObj.isHovered){
          attrs.ammo = ammos;
      }
      /*
      if (_intObj.isSuspended) {
        // If the object is held by only one hand and that holding hand stops tracking, the
        // object is "suspended." InteractionBehaviour provides suspension callbacks if you'd
        // like the object to, for example, disappear, when the object is suspended.
        // Alternatively you can check "isSuspended" at any time.
        targetColor = suspendedColor;
      }

      // We can also check the depressed-or-not-depressed state of InteractionButton objects
      // and assign them a unique color in that case.
      if (_intObj is InteractionButton && (_intObj as InteractionButton).isPressed) {
        targetColor = pressedColor;
      }

      // Lerp actual material color to the target color.
      _material.color = Color.Lerp(_material.color, targetColor, 30F * Time.deltaTime);
    }
    */
    }
}
