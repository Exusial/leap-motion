﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gun_manager : MonoBehaviour
{
    protected GameObject[] all_guns;
    public int index;
    public bool is_change = false;
    // public int now_ammo;
    private int len;
    private bool can_change = true;
    void Awake(){
        all_guns = GameObject.FindGameObjectsWithTag("guns");
        index = 0;
        len = all_guns.GetLength(0);
    }
    // Start is called before the first frame update
    void Start()
    {
        print("All_GUNS_shape");
        all_guns[0].SetActive(true);
        for(int i=1;i<len;i++){
            all_guns[i].SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void change_gun(){
        print("In");
        is_change = true;
        if(can_change){
            all_guns[index].SetActive(false);
            all_guns[(index+1)%len].SetActive(true);
            can_change = false;
            // now_ammo = all_guns[index].GetComponent<gun_attr>().ammo;
            index += 1;
            index = index % len;
        }
    }

    public void enable_change(){
        print("Out");
        is_change = false;
        can_change = true;
    }
}
