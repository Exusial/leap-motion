﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using System;

public class FireManager : MonoBehaviour
{
    public int gun_nums;
    public byte need_change;
    private Socket all_socket=null;
    private IPAddress my_ip = null;
    private Socket pc_socket = null;
    private GameObject righthand = null;
    private GameObject[] all_guns = null; // save all guns
    private int[] gun_ammos;
    private int index; // which gun is being used
    private int ammo; // ammo of gun being used
    private int counter = 0;
    private gun_manager manager;
    private ReleaseFire rel;
    private byte[] data;
    // private bool is_change = false;
    // Start is called before the first frame update
    void Awake(){
        parse_gun();
        data = new byte[22];
        righthand = GameObject.FindGameObjectWithTag("righthand");
        rel = GameObject.FindObjectOfType<ReleaseFire>();
        if(righthand == null){
            print("what i've done is junk");
        }
    }
    void prepare(){
        while(pc_socket == null){
            pc_socket = all_socket.Accept();
        }
        print("complete connecting");
    }
    void Start()
    {
        all_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        my_ip = IPAddress.Parse("192.168.43.230");
        all_socket.Bind(new IPEndPoint(my_ip,8888));
        all_socket.Listen(20);
        manager = FindObjectOfType<gun_manager>();
        new Thread(prepare).Start();
    }

    // Update is called once per frame
    void Update()
    {
        try{
            if(pc_socket != null && pc_socket.Connected){
                if(counter%5==0)
                    process_ammo_and_transform();
                counter = (counter+1)%5;
            }
            else{
                // print("Disconnected!!");
            }
        }
        catch(SocketException ex){
            pc_socket = null;
            new Thread(prepare).Start();
        }
    }

    //Init gun ammos
    void parse_gun(){
        gun_ammos = new int[gun_nums];
        all_guns = GameObject.FindGameObjectsWithTag("guns");
        for(int i=0;i<gun_nums;i++){
            gun_ammos[i] = all_guns[i].GetComponent<gun_attr>().ammo;
        }
    }

    void process_ammo_and_transform(){

        index = manager.index;
        byte state = all_guns[index].GetComponent<TcpFire>().state;
        Transform gun_t = all_guns[index].transform;
        Vector3 pos = gun_t.position;
        Quaternion rotate = gun_t.rotation;
        //byte[] px = BitConverter.GetBytes(pos.x);
        //byte[] py = BitConverter.GetBytes(pos.y);
        //byte[] pz = BitConverter.GetBytes(pos.z);
        byte[] rx = BitConverter.GetBytes(rotate.x);
        byte[] ry = BitConverter.GetBytes(rotate.y);
        byte[] rz = BitConverter.GetBytes(rotate.z);
        byte[] rw = BitConverter.GetBytes(rotate.w);
        data = new byte[32];
        data[0] = 65;
        data[1] = state;
        if(manager.is_change == true){
            data[2] = 1;
            manager.is_change = false;
        }
        else
            data[2] = 0;
        for(int i=0;i<4;i++){
            data[3+i] = rx[i];
            data[7+i] = ry[i];
            data[11+i] = rz[i];
            data[15+i] = rw[i];
        }
        if(need_change == 1){
            data[19] = 1;
            need_change = 0;
        }
        else
            data[19] = 0;
        if(rel.is_releasing){
            data[20] = 1;
            rel.is_releasing = false;
        }
        else{
            data[20] = 0;
        }
        data[21] = 65;
        new Thread(send_data).Start();
    }
    void send_data(){
        pc_socket.Send(data,0);
    }
}
